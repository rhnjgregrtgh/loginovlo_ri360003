var arr = [1, 2, 3, 3, 4, 5, 6, 3, 3, 3, 8];
var element = null;
var counter = 0;
arr.forEach(function(value, index, array) {
  if (element === value) {
    if (counter === 0) {
      ++counter;
    }
    ++counter;
  } else if (counter === 0) {
    element = value;
  }
});
console.log(counter, element);